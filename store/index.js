import Vue from "vue";
import Vuex from "vuex";
import actions from "./actions/actions";
import mutations from "./mutations/mutations";
import state from "./state";
import getters from "./getters/getters";

Vue.use(Vuex)

const store  = () => new Vuex.Store({
    state,
    mutations,
    actions,
    getters
});

export default store ;
