import axios from "axios";

const actions = {
    getUsers({commit}) {
        axios({
            method: "get",
            url: 'https://jsonplaceholder.typicode.com/users',
        })
            .then(res => {
                if(res.data.length){
                    commit('SET_USER', res.data || null);
                }
            })
    },

    updateUsers({commit}, data) {
        commit('SET_USER', data);
    }
};

export default actions;