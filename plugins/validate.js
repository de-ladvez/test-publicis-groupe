const Joi = require('joi');

const vEmail = Joi.string().email({tlds: false}).required().error(errors => {
    errors[0].message = "Введите корректный email";
    return errors;
});
const vPassword = Joi.string().required().min(6).error(errors => {
    errors[0].message = "Введите корректный пароль, не менее 6 знаков";
    return errors;
});

function resError (res) {
    return {errorMess: res.error && res.error.message || ""};
}

export const validEmail = function (val) {
    const res = vEmail.validate(val);
    return resError(res);
};

export const validPassword = function (val) {
    const res = vPassword.validate(val);
    return resError(res);
};

